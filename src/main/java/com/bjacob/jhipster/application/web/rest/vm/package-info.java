/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bjacob.jhipster.application.web.rest.vm;
